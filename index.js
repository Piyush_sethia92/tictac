/**
 * This program is a boliler plate code for the famous tic tac toe game
 * Here box represents one placeholder for either X or a 0
 * We have a 2D array to represent the arrangement of X or O is a grid
 * 0 -> empty box
 * 1 -> box with X
 * 2 -> box with O
 * 
 * Below are the tasks which needs to be completed
 * Imagine you are playing with Computer so every alternate move should be by Computer
 * X -> player
 * O -> Computer
 * 
 * Winner has to be decided and has to be flashed
 * 
 * Extra points will be given for the Creativity
 * 
 * Use of Google is not encouraged
 * 
 */
var grid = [];
const GRID_LENGTH = 3;
var turn = 'X';
var turnCount = 0;
var isGamerunning = false;


function initializeGrid() {
    for (let colIdx = 0;colIdx < GRID_LENGTH; colIdx++) {
        let tempArray = [];
        for (let rowidx = 0; rowidx < GRID_LENGTH;rowidx++) {
            tempArray.push(0);
        }
        grid.push(tempArray);
    }
}

function getRowBoxes(colIdx) {
    let rowDivs = '';
    
    for(let rowIdx=0; rowIdx < GRID_LENGTH ; rowIdx++ ) {
        let additionalClass = 'darkBackground';
        let content = '';
        const sum = colIdx + rowIdx;
        if (sum%2 === 0) {
            additionalClass = 'lightBackground'
        }
        const gridValue = grid[colIdx][rowIdx];
        if(gridValue === 1) {
            content = '<span class="cross">X</span>';
        }
        else if (gridValue === 2) {
            content = '<span class="cross">O</span>';
        }
        rowDivs = rowDivs + '<div colIdx="'+ colIdx +'" rowIdx="' + rowIdx + '" class="box ' +
            additionalClass + '">' + content + '</div>';
    }
    return rowDivs;
}

function getColumns() {
    let columnDivs = '';
    for(let colIdx=0; colIdx < GRID_LENGTH; colIdx++) {
        let coldiv = getRowBoxes(colIdx);
        coldiv = '<div class="rowStyle">' + coldiv + '</div>';
        columnDivs = columnDivs + coldiv;
    }
    return columnDivs;
}

function renderMainGrid() {
    const parent = document.getElementById("grid");
    const columnDivs = getColumns();
    parent.innerHTML = '<div class="columnsStyle">' + columnDivs + '</div>';
}


function checkValue(v1, v2, v3) {
    if (!v1 || !v2 || !v3) {
        return false;
    } 
    let winner = v1 === 1 ? 'X' : 'O';
    return winner;
}

function checkCurrentStatus() {
    let status;
    if (grid[0][0] === grid[0][1] && grid[0][1] === grid[0][2] ) {
        status = checkValue(grid[0][0], grid[0][1], grid[0][2]);
        if (status) {
            window.alert(status + ' wins');
            return true;
        }
    } else if ( grid[1][0] === grid[1][1] && grid[1][1] === grid[1][2] ) {
        status = checkValue(grid[1][0], grid[1][1], grid[1][2]);
        if (status) {
            window.alert(status + ' wins');
            return true;
        }
    } else if ( grid[2][0] === grid[2][1] && grid[2][1] === grid[2][2] ) {
        status = checkValue(grid[2][0], grid[2][1], grid[2][2]);
        if (status) {
            window.alert(status + ' wins');
            return true;
        }
    } else if ( grid[0][0] === grid[1][0] && grid[1][0] === grid[2][0] ) {
        status = checkValue(grid[0][0], grid[1][0], grid[2][0]);
        if (status) {
            window.alert(status + ' wins');
            return true;
        }
    } else if ( grid[0][1] === grid[1][1] && grid[1][1] === grid[2][1] ) {
        status = checkValue(grid[0][1], grid[1][1], grid[2][1]);
        if (status) {
            window.alert(status + ' wins');
            return true;
        }
    } else if ( grid[0][2] === grid[1][2] && grid[1][2] === grid[2][2] ) {
        status = checkValue(grid[0][2], grid[1][2], grid[2][2]);
        if (status) {
            window.alert(status + ' wins');
            return true;
        }
    } else if ( grid[0][0] === grid[1][1] && grid[1][1] === grid[2][2] ) {
        status = checkValue(grid[0][0], grid[1][1], grid[2][2]);
        if (status) {
            window.alert(status + ' wins');
            return true;
        }
    } else if ( grid[0][2] === grid[1][1] && grid[1][1] === grid[2][0] ) {
        status = checkValue(grid[0][2], grid[1][1], grid[2][0]);
        if (status) {
            window.alert(status + ' wins');
            return true;
        }
    }
    return false;
}


function onBoxClick(eve) {
    var rowIdx = this.getAttribute("rowIdx");
    var colIdx = this.getAttribute("colIdx");
    let newValue;
    if (eve && eve.target && !eve.target.innerHTML && !isGamerunning) {
        turnCount++;
        if (turn === 'X') {
            turn = 'O';
            newValue = 1;
        } else {
            turn = 'X';
            newValue = 2;   
        }
        grid[colIdx][rowIdx] = parseInt(newValue);
        if (turnCount > 4) {
             isGamerunning = checkCurrentStatus();
        }
        if (!isGamerunning) {
            renderMainGrid();
            addClickHandlers();
        }
    }
}

function addClickHandlers() {
    var boxes = document.getElementsByClassName("box");
    for (var idx = 0; idx < boxes.length; idx++) {
        boxes[idx].addEventListener('click', onBoxClick, false);
    }
}

function initRestart() {
    window.location.reload();
}

initializeGrid();
renderMainGrid();
addClickHandlers();
